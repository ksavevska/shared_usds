#launch Isaac Sim before any other imports

#default first two lines in any standalone application

# import pinocchio as pin
from omni.isaac.kit import SimulationApp

# simulation_app = SimulationApp({"headless": False, "physics_gpu": 0}, experience='/home/ksavevska/.local/share/ov/pkg/isaac_sim-2023.1.1/apps/omni.isaac.sim.python.gym.kit') # we can also run as headless. # we can also run as headless.
simulation_app = SimulationApp({"headless": False})


from omni.isaac.core import World
from omni.isaac.core.articulations import Articulation
from omni.isaac.core.utils.prims import define_prim
from pxr import UsdPhysics
from omni.isaac.core.utils.stage import get_current_stage, is_stage_loading, add_reference_to_stage
from omni.isaac.core import SimulationContext

import numpy as np



world = World()
world.scene.add_default_ground_plane(prim_path="/World/defaultGroundPlane")

# asset_path = "talos_full_v2_collision_2022.2.1.usd"
asset_path = "talos_full_v2_collision_2023.1.1.usd"

add_reference_to_stage(usd_path=asset_path, prim_path="/World/talos")
talos = Articulation(prim_path="/World/talos", 
                                     name="talos", 
                                     position=np.array([0.0, 0.0, 1.2]),
                                     enable_dof_force_sensors=True)
world.scene.add(talos)
stage = simulation_app.context.get_stage()

world.reset()

for i in range(100):
    sensor_joint_forces = talos.get_measured_joint_forces()

    left_joint_path = "/World/talos/leg_left_6_link/leg_left_sole_fix_joint"
    left_joint = UsdPhysics.Joint.Get(stage, left_joint_path)
    body_l_path = left_joint.GetBody1Rel().GetTargets()[0]
    body_l_name = stage.GetPrimAtPath(body_l_path).GetName()
    child_link_index_left = talos._articulation_view.get_link_index(body_l_name)
    left_joint_link_id =  child_link_index_left


    right_joint_path = "/World/talos/leg_right_6_link/leg_right_sole_fix_joint"
    right_joint = UsdPhysics.Joint.Get(stage, right_joint_path)
    body_r_path = right_joint.GetBody1Rel().GetTargets()[0]
    body_r_name = stage.GetPrimAtPath(body_r_path).GetName()
    child_link_index_right = talos._articulation_view.get_link_index(body_r_name)
    right_joint_link_id =  child_link_index_right

    print("joint link IDs", left_joint_link_id, right_joint_link_id)
    
    print("LEFT SOLE")
    print(sensor_joint_forces[left_joint_link_id])
    print("RIGHT SOLE")
    print(sensor_joint_forces[right_joint_link_id])
    
    world.step(render=True) # execute one physics step and one rendering step

simulation_app.close() # close Isaac Sim
